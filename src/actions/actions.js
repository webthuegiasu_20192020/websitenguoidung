//import {actions} from 'react-redux-form'

//User's Actions
export const GET_USER = "GET_USER";
export const GET_TOKEN = "GET_TOKEN";
export const REDIRECT_API = "REDIRECT_API";
export const GET_ALL_USER = "GET_ALL_USER";
export const LOG_OUT = "LOG_OUT";

export function getUser(data) {
  return {
    type: "GET_USER",
    data
  };
}

export function logOut() {
  return {
    type: "LOG_OUT"
  };
}

export function getToken(token) {
  return {
    type: "GET_TOKEN",
    token
  };
}

export function redirectAPI(URL) {
  return {
    type: "REDIRECT_API",
    URL
  };
}

export function getAllUser(user) {
  return {
    type: "GET_ALL_USER",
    user
  };
}

export const postProtected = (URL, redirectToURL, data) => {
  return (dispatch, getState) => {
    const state = getState();
    const { user } = state;
    fetch(URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${user.token}`
      },
      body: JSON.stringify({
        data
      }),
      redirect: "follow"
    })
      .then()
      .catch(error => {
        console.log(error);
      });
    dispatch(redirectAPI(redirectToURL));
  };
};

export const getProtected = (URL, nextAction) => {
  return (dispatch, getState) => {
    const state = getState();
    const { user } = state;
    fetch(URL, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${user.token}`
      },
      redirect: "follow"
    })
      .then(res => res.json())
      .then(res => {
        dispatch(nextAction(res.data));
      })
      .catch(error => {
        console.log(error);
      });
  };
};
