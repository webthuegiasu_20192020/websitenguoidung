const tutorSpecialty = {
  grade: {
    grade_1: false,
    grade_2: false,
    grade_3: false,
    grade_4: false,
    grade_5: false,
    grade_6: false,
    grade_7: false,
    grade_8: false,
    grade_9: false,
    grade_10: false,
    grade_11: false,
    grade_12: false
  },
  subject: {
    math: false,
    literature: false,
    chemistry: false,
    physics: false,
    biology: false,
    english: false
  }
};

export default tutorSpecialty;
