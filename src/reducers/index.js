import detailForm from "./detailForm";
import tutorSpecialty from "./tutorSpecialty";
import { combineReducers } from "redux";
import { createForms } from "react-redux-form";
import user from "./user";
import apiAction from "./api";
import initialStateUser from "./initialStateUser";
import tempAdminUser from "./tempAdminUser";

export default combineReducers({
  user,
  apiAction,
  ...createForms({
    detailForm: detailForm,
    tutorSpecialty: tutorSpecialty,
    initialStateUser: initialStateUser,
    tempAdminUser: tempAdminUser
  })
});
