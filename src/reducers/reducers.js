import {combineReducers} from 'redux'
import {createForms} from 'react-redux-form';
import {
    GET_USER,
    GET_TOKEN,
    GET_ALL_USER,
    REDIRECT_API,
} from '../actions/actions'


//Tạo thông tin bắt đầu cho người dùng
const initialStateUser = {
  firstname: '',
  lastname: '',
  email: '',
  username: '',
  password: ''
}

const user = (state = {data: null, token: null}, action) => {
  switch (action.type) {
    case GET_USER:
      return {
        data: action.data,
        token: state.token
      };
    case GET_TOKEN:
      return {
        ...state,
        token: action.token
      };
    case GET_ALL_USER:
      return action.user;
    default:
      return state;
  }
}

const apiAction = (state = {isRedirect: false, URL:''}, action) => {
  switch (action.type) {
    case REDIRECT_API:
      return {
        isRedirect: true,
        URL: action.URL
      };
    default:
      return state;
  }
}

export default combineReducers({
    user,
    apiAction,
    ...createForms({
      userForm: initialStateUser
    })
});
  