import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import UserLogin from "./UserAccount/UserLoginCon";
import UserSignup from "./UserAccount/UserSignupCon";
import DetailsPage from "./DetailsPage";
import TutorSpecialty from "./TutorSpecialty";
import ListOfTutors from "./ListOfTutors";
import ListOfTutorsRate from "./ListOfTutorsRate";
import ListOfTutorsSubject from "./ListOfTutorsSubject";
import ListOfTutorsAddress from "./ListOfTutorsAddress";
import AdminPage from "./AdminPage";
import AddAdminPage from "./AddAdminPage";
import HomePage from "./HomePage";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <Switch>
        {/* Basic User functions (Does not require logging in) */}
        <Route exact path="/" component={HomePage}></Route>
        <Route path="/home" component={HomePage}></Route>
        <Route path="/login" component={UserLogin}></Route>
        <Route path="/register" component={UserSignup}></Route>
        <Route path="/listoftutors" component={ListOfTutors}></Route>
        <Route path="/listoftutorsbyrate" component={ListOfTutorsRate}></Route>
        <Route path="/listoftutorsbysubject" component={ListOfTutorsSubject}></Route>
        <Route path="/listoftutorsbyaddress" component={ListOfTutorsAddress}></Route>
        
        {/* Admin functions */}
        <Route exact path="/admin" component={AdminPage}></Route>
        <Route exact path="/admin/add" component={AddAdminPage}></Route>

        {/* Tutor functions */}
        <Route path="/specialty" component={TutorSpecialty}></Route>
        <Route path="/detail" component={DetailsPage}></Route>


        {/* Student functions */}

      </Switch>
    </Router>
  );
}

export default App;
