import { connect } from "react-redux";
import { actions } from "react-redux-form";
import { postProtected } from "../../actions/actions";
import TutorSpecialty from "./TutorSpecialty";

const mapStateToProps = state => {
  const { apiAction } = state;
  return {
    isRedirect: apiAction.isRedirect,
    URL: apiAction.URL
  };
};

const mapDispatchToProps = dispatch => ({
  handleSubmit: data => {
    console.log(data);
    dispatch(postProtected("/specialty", "/home", data));
  },
  handleClick: model => {
    return dispatch => {
      dispatch(actions.toggle(model));
    };
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TutorSpecialty);
