import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import BeginingNavBar from "../BeginingNavBar";
import {Container, Card, Button, Form} from "react-bootstrap";
import Footer from '../Footer'

const hireTutorForm = props => {
    return <Form.Control {...props} />;
};

const HireTutor = ({handleSubmit, handleClick, match, token}) => {
    const [data, setData] = useState({
        tutor_id: "",
        tutor_name: "",
        description: "",
        address: "",
        skills: "",
    });
    
    useEffect(() => {
        const fetchData = async (match, token) => {
          fetch(`/users/contract/post/tutor=${match.params.id}`, {
            headers: {
              "Content-Type": "application/json",
              Accept: "application/json",
              Authorization: `Bearer ${token}`
            }
          })
            .then(res => res.json())
            .then(setData);
        };
        fetchData(match, token);
    }, [match, token]);
    
    const isRedirect = useState(false);
  
    if (isRedirect) {
      return <Redirect to="/users/contract"/>;
    }
  
    return (
        <div>
            <BeginingNavBar></BeginingNavBar>
            <div className='popup'>
            <div className='popup_inner'>
                <Container>
                    <Form onSubmit={event => handleSubmit(event)}>
                        <Card>
                            <Card.Header className="text-center"><h1>Hire this tutor for a course</h1></Card.Header>
                            <Card.Body>
                                    <Form inline>
                                        <Form.Label>
                                            Tutor: {` ` + data.tutor_name}
                                        </Form.Label>
                                    </Form>

                                    <Form inline>
                                        <Form.Label>
                                            Description: {` ` + data.description}
                                        </Form.Label>
                                    </Form>

                                    <Form inline>
                                        <Form.Label>
                                            Address: {` ` + data.address}
                                        </Form.Label>
                                    </Form>

                                    <Form inline>
                                        <Form.Label>
                                            Tuition Fee: {` ` + data.tutor_name}
                                        </Form.Label>
                                    </Form>

                                    <Form>
                                        <Form.Label>Skill: {data.skill}</Form.Label>
                                            <Form.Control as="select" style={{width: '15%'}}>
                                                {
                                                    data.skills.map(skill =>
                                                        (
                                                            <option component={hireTutorForm} model='.skill' changeAction={model => handleClick(model)}>
                                                                {skill.name}
                                                            </option>
                                                        )
                                                    )
                                                }
                                            </Form.Control>
                                    </Form>

                                    <Form>Hours:
                                        <Form.Control as="textarea" rows="1" style={{width: '25%'}} model='.hours' component={hireTutorForm}/>
                                    </Form>

                                    <Form inline>Contract's condition from Student:</Form>
                                    <Form>
                                        <Form.Control as="textarea" rows="3" model='.condition' component={hireTutorForm}/>
                                    </Form>

                                    <div className="text-center">
                                        <Button className="popupBtn" variant="success" type='submit' onSubmit={event => handleSubmit(event)}>Hire this Tutor</Button>
                                        <br></br>
                                        <Button className="popupBtn" variant="dark" href="/listoftutors">Go back</Button>
                                    </div>
                            </Card.Body>
                        </Card>
                    </Form>
                </Container>
            </div>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default HireTutor