import { connect } from "react-redux";
import { postProtected } from "../../actions/actions";
import DetailsPage from "./DetailsPage";

const mapStateToProps = state => {
  const { apiAction } = state;
  return {
    isRedirect: apiAction.isRedirect,
    URL: apiAction.URL
  };
};

const mapDispatchToProps = dispatch => ({
  handleSubmit: data => {
    console.log(data);
    dispatch(postProtected("/detail", "/specialty", data));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(DetailsPage);
