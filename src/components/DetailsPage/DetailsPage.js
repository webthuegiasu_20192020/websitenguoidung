import React from "react";
import { Redirect } from "react-router-dom";
import BeginingNavBar from "../BeginingNavBar";
import { Form, Button } from "react-bootstrap";
import { Form as ReduxForm, Control, Errors } from "react-redux-form";
import "./DetailsPage.css";

const bootstrapFormControl = props => {
  return <Form.Control {...props} />;
};

const detailsPage = ({ handleSubmit, isRedirect, URL }) => {
  const link = isRedirect => {
    if (isRedirect) {
      return <Redirect to={URL} />;
    }
    return null;
  };
  return (
    <div>
      <BeginingNavBar />
      <div className="form">
        <div className="Content">
          <ReduxForm model="detailForm" onSubmit={v => handleSubmit(v)}>
            <Form.Group controlId="formBasicCreditCard">
              <Form.Label>Credit card number</Form.Label>
              <Control.text
                model=".creditCard"
                component={bootstrapFormControl}
                type="text"
                validators={{ isRequired: val => val && val.length }}
              />
              <Errors
                model="detailForm.creditCard"
                className="alert alert-danger"
                show={field => field.touched && !field.focus}
                messages={{
                  isRequired: "Please provide your credit card info"
                }}
              />
            </Form.Group>
            <Form.Group controlId="formBasicAddress">
              <Form.Label>Address</Form.Label>
              <Control.text
                model=".address"
                component={bootstrapFormControl}
                type="text"
                validators={{ isRequired: val => val && val.length }}
              />
              <Errors
                model="detailForm.address"
                className="alert alert-danger"
                show={field => field.touched && !field.focus}
                messages={{
                  isRequired: "Please provide your currennt address"
                }}
              />
            </Form.Group>
            <Form.Group controlId="formBasicDescription">
              <Form.Label>Description</Form.Label>
              <Control.text
                model=".description"
                component={bootstrapFormControl}
                as="textarea"
                rows="3"
                validators={{ isRequired: val => val && val.length }}
              />
              <Errors
                model="detailForm.description"
                className="alert alert-danger"
                show={field => field.touched && !field.focus}
                messages={{
                  isRequired:
                    "Please provide a basic description about yourself"
                }}
              />
            </Form.Group>
            <div className="w-100" />
            <Button variant="primary" type="submit">
              Update
            </Button>
          </ReduxForm>
        </div>
      </div>
      {link(isRedirect)}
    </div>
  );
};

export default detailsPage;
