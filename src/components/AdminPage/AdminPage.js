import React, { useState, useEffect } from "react";
import { Redirect, Link } from "react-router-dom";
import BeginingNavBar from "../BeginingNavBar";
import { Table } from "react-bootstrap";
import "./AdminPage.css";

const AdminPage = ({ isRedirect, URL, token }) => {
  let [dataAdmin, setData] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      fetch(`/admin`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          Authorization: `Bearer ${token}`
        }
      })
        .then(res => res.json())
        .then(setData);
    };
    fetchData();
  }, [token]);
  const link = isRedirect => {
    if (isRedirect) {
      return <Redirect to={URL} />;
    }
    return null;
  };
  const dataTable = dataAdmin.map(value => {
    return (
      <tr>
        <td>{value.id}</td>
        <td>{value.username}</td>
        <td>{value.email}</td>
      </tr>
    );
  });
  return (
    <div>
      <BeginingNavBar />
      <div className="form">
        <div className="Content">
          <Link to="/admin/add" className="btn btn-success mb-3">
            Add Amin
          </Link>
          <Table striped bordered hover variant="dark">
            <thead>
              <tr>
                <th>#</th>
                <th>Username</th>
                <th>Email</th>
              </tr>
            </thead>
            <tbody>{dataTable}</tbody>
          </Table>
        </div>
      </div>
      {link(isRedirect)}
    </div>
  );
};

export default AdminPage;
