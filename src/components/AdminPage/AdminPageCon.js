import { connect } from "react-redux";
import AdminPage from "./AdminPage";

const mapStateToProps = state => {
  const { apiAction, user } = state;
  return {
    isRedirect: apiAction.isRedirect,
    URL: apiAction.URL,
    token: user.token
  };
};

export default connect(mapStateToProps)(AdminPage);
