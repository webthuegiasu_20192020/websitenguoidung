import React from 'react';
import {Button, Navbar, Jumbotron, Nav, Form, FormControl, Card, CardDeck, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import './HomePage.css';
import bgimage from '../images/homepagecover.png'
import tutorimage from '../images/tutor.png'
import Footer from '../Footer'


const ColoredDivideLine = ({color}) => (
    <hr
        style={{
            color: color,
            backgroundColor: color,
            height: 0.2
        }}
    />
);

class HomePage extends React.Component {
    
    render()
    {
        return (
            <div>
                <div className="body">
                    <Navbar expand="lg" className="BeginNavBar">
                        <Navbar.Brand className="SignupNavBrand"><Link to="/home" style={{textDecoration: "none", color: "forestgreen"}}>Online Tutors Services</Link></Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav" className="SignupNavCollapse">
                            <Nav className="mr-auto"></Nav>

                            <Nav>
                                <Form className="navBtn" inline>
                                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                                    <Button variant="outline-success">Search</Button>
                                </Form>
                                <div className="navBtn">
                                    <Button className="btn loginBtn" variant="outline-success" href="/login">Log In</Button>
                                </div>
                                <div className="navBtn">
                                    <Button className="btn signupBtn" variant="outline-success" href="/register">Sign Up</Button>
                                </div>
                            </Nav>
                        </Navbar.Collapse>
                    </Navbar>

                    <Jumbotron className="text-center backgroundImage" style={{ backgroundImage: `url(${bgimage})`, backgroundSize: 'cover'}}>
                        <h1 className="backgroundTitle">Hire tutors.</h1>
                        <p className="backgroundDescription">
                            Get matched to the best talent in minutes through our
                            <br></br>
                            network of various skilled professors and professional tutors.
                        </p>
                        <p>
                            <Button className="getStartedBtn" variant="success" href="/register">Get Started</Button>
                        </p>
                    </Jumbotron>

                    <CardDeck className="cardDeck">
                        <Card className="text-center">
                            <Button variant="success" href="/listoftutors">
                                <Card.Body>
                                    <div className="cardImageHeader" style={{ backgroundImage: `url(${tutorimage})`, backgroundSize: 'cover'}}></div>
                                    <Card.Title>Look for a Tutor</Card.Title>
                                    <Card.Text>
                                        Need help improving your knowledge of certain subjects ?
                                        <br></br>
                                        Look for a Tutor varied from certain subjects which you want to improve.
                                        <br></br>
                                    </Card.Text>
                                </Card.Body>
                            </Button>
                        </Card>
                    </CardDeck>

                    <div className="tutorsContainer">
                        <Container>
                            <ColoredDivideLine color="lightgray"/>
                                <h1 className="text-center">Top Tutors</h1>
                            <ColoredDivideLine color="lightgray"/>

                            <br></br><br></br>

                            <div>
                                <Card className="text-center">
                                    <Button variant="success" href="/listoftutors">
                                        <Card.Body>
                                            <Card.Title>ABC</Card.Title>
                                            <Card.Text>
                                                AAAAAAAAAAAAAAAAAAA
                                                <br></br>
                                                BBBBBBBBBBBBBBBBBBB
                                                <br></br>
                                            </Card.Text>
                                        </Card.Body>
                                    </Button>
                                </Card>
                            </div>
                        </Container>
                    </div>

                    <div>
                        <Footer></Footer>
                    </div>

                </div>
            </div>
        )
    }
}

export default HomePage;