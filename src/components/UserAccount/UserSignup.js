import React from 'react';
import {Button, Form, Navbar, Modal, Nav} from 'react-bootstrap';
import {Link, Redirect} from 'react-router-dom';
import {Form as ReduxForm, Control} from 'react-redux-form';
import './UserSignup.css';
import Footer from '../Footer'

const bootstrapForm = props => {
  return <Form.Control {...props} />;
};

const UserSignup = ({handleSubmit, handleClick, isRedirect, URL}) => {
  const link = isRedirect => {
    if (isRedirect) {
      return <Redirect to={URL}/>;
    }
    return <div/>;
  };

  return (
    <div>
      <div>
        <Navbar expand="lg" className="BeginNavBar">
          <Navbar.Brand className="SignupNavBrand"><Link to="/home" style={{textDecoration: "none", color: "forestgreen"}}>Online Tutors Services</Link></Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav" className="SignupNavCollapse">
              <Nav className="mr-auto"></Nav>
                Already have an account? 
                <Link to="/login" className="LinkLoginFromSignUp">Login</Link>
          </Navbar.Collapse>
        </Navbar>
      </div>

      <div className='SignupForm'>
        <ReduxForm model='userForm' onSubmit={event => handleSubmit(event)}>
          <Modal.Dialog style={{width:"300%"}}>
            <Modal.Header>
              <Modal.Title>SIGN UP</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <Form.Group>
                  <Form.Label>First Name</Form.Label>
                  <Control.text
                    autoFocus
                    name='firstname'
                    model='.firstname'
                    component={bootstrapForm}
                    placeholder='Enter your First Name'
                  />
                </Form.Group>

                <Form.Group>
                  <Form.Label>Last Name</Form.Label>
                  <Control.text
                    name='lastname'
                    model='.lastname'
                    component={bootstrapForm}
                    placeholder='Enter your Last Name'
                  />
                </Form.Group>

                <Form.Group>
                  <Form.Label>Email</Form.Label>
                  <Control.text
                    name='email'
                    model='.email'
                    component={bootstrapForm}
                    placeholder='Enter your Email'
                  />
                </Form.Group>

                <Form.Group>
                  <Form.Label>Username</Form.Label>
                  <Control.text
                    name='username'
                    model='.username'
                    component={bootstrapForm}
                    placeholder='Enter your Username'
                  />
                </Form.Group>
        
                <Form.Group>
                  <Form.Label>Password</Form.Label>
                  <Control.text
                    name='password'
                    model='.password'
                    component={bootstrapForm}
                    placeholder='Enter your Password'
                    type='password'
                  />
                </Form.Group>

                <Form.Group>
                  <Form.Label>I want to</Form.Label>
                  <Form.Control as="select">
                    <option 
                      name='usertype'
                      model='.usertype.1'
                      component={bootstrapForm}
                      changeAction={model => handleClick(model)}>
                        Hire a Tutor
                    </option>
                    <option 
                      name='usertype'
                      model='.usertype.2'
                      component={bootstrapForm}
                      changeAction={model => handleClick(model)}>
                        Work as a Tutor
                    </option>
                  </Form.Control>
                </Form.Group>       
            </Modal.Body>

            <Modal.Footer>
              <Button className='btn btn-primary Signupbtn' type='submit' block>
                Sign Up
              </Button>

              <Link to='/login' className='btn Cancel'>Cancel</Link>
              </Modal.Footer>
          </Modal.Dialog>
        </ReduxForm>
        <div>{link(isRedirect)}</div>
      </div>
      <Footer></Footer>
    </div>
  );
}

export default UserSignup;