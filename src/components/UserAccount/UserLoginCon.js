import {connect} from 'react-redux';
import {getAllUser, redirectAPI} from '../../actions/actions';
import UserLogin from './UserLogin';

const mapStateToProps = state => {
  const {apiAction} = state;
  return {
    isRedirect: apiAction.isRedirect,
    URL: apiAction.URL
  };
};

const mapDispatchToProps = dispatch => ({
  handleSubmit: data => {
    console.log(data);
    fetch('/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        firstname: data.firstname,
        lastname: data.lastname,
        email: data.email,
        username: data.username,
        password: data.password,
        usertype: data.usertype 
      }),
      redirect: 'follow'
    })
      .then(res => res.json())
      .then(res => {
        console.log(res);
        dispatch(getAllUser({data: res.user, token: res.token}));
        if (data.usertype === 1)
        {
          dispatch(redirectAPI('/listoftutors'));
        }
        else if (data.usertype === 2)
        {
          dispatch(redirectAPI('/detail'));
        }
        else if (data.usertype === 0)
        {
          dispatch(redirectAPI('/admin'));
        }
      })
      .catch(error => {
        console.log(error);
      });
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserLogin);