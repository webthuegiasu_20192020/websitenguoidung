import React from "react";
import { Link } from "react-router-dom";
import BeginingNavBar from "../BeginingNavBar";
import {Container, Row, Col, Card, Button} from "react-bootstrap";
import "./ListOfTutors.css";
import Footer from '../Footer'


class ListOfTutorsAddress extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            tutorsbyaddress: []
        }

        fetch("http://localhost:5000/tutorsbyaddress", //Select ở bảng users và tutorsdetail theo address được ASCEND 
        {
            method: 'GET',
            })
              .then(res => res.json())
              .then(tutorspecialityaddress => {
                console.log(tutorspecialityaddress)
                this.setState({tutorspecialityaddress})
              })
              .catch(error => {
                console.log(error);
        });
    }

    render() {
        const {tutorsbyaddress} = this.state;

        return (
            <div>
                <BeginingNavBar></BeginingNavBar>
                <Container>
                    <Row>
                        <Col xs={6} md={4} id="containerSidebar">
                            <Card bg="success" text="white" style={{width: '18rem'}}>
                                <Card.Body>
                                    <Card.Title>Category</Card.Title>
                                    <div>
                                        <Link to="/listoftutors" style={{textDecoration: "none", color: "white"}}>All Categories</Link>
                                    </div>
                                    <div>
                                        <Link to="/listoftutorsbysubject" style={{textDecoration: "none", color: "white"}}>Sort by Subjects</Link>
                                    </div>
                                    <div>
                                        <Link to="/listoftutorsbyrate" style={{textDecoration: "none", color: "white"}}>Sort by Hourly Rate</Link>
                                    </div>
                                    <div>
                                        <Link to="/listoftutorsbyaddress" style={{textDecoration: "none", color: "white"}}>Sort by Address</Link>
                                    </div>
                                </Card.Body>
                            </Card>
                        </Col>
                        
                        <Col xs={12} md={8} id="containerContent">
                        {
                                tutorsbyaddress.map(tutor => 
                                (
                                    <Card border="secondary" style={{width: '50rem'}} className="listContent"  key={tutor._id} href={`/users/${tutor._id}`}>
                                        <Card.Body>
                                            <Card.Title>{tutor.username}</Card.Title>
                                            <Card.Subtitle className="mb-2 text-muted">Specialty: {`,` + tutor.skills}</Card.Subtitle>
                                            <Card.Text>
                                            {tutor.description}
                                            <br></br>
                                            Address: {tutor.address}
                                            <br></br>
                                            Tuition Fee: {tutor.rate} /h
                                            </Card.Text>
                                            <Button variant="outline-success" href={`/users/contract/post/tutor=${tutor._id}`}>Hire Tutor</Button>
                                            <Button variant="danger" style={{marginLeft: "5px"}}>Favorite Tutor</Button>
                                        </Card.Body>
                                    </Card>
                                    )
                                )
                        }
                        </Col>
                    </Row>
                </Container>
                <Footer></Footer>
            </div>
        )
    }
}
export default ListOfTutorsAddress;